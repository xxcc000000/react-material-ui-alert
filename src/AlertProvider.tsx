import React, {
    useState,
} from 'react';
import {
    Button,
    createStyles,
    Dialog, DialogActions,
    Theme,
    Typography, useTheme,
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { FontAwesomeIcon as Icon } from '@fortawesome/react-fontawesome';
import { IconProp } from '@fortawesome/fontawesome-svg-core';
import {
    faQuestion,
    faCheck,
    faExclamation,
    faTimes,
} from '@fortawesome/pro-regular-svg-icons';

export type Alert = {
    show: (option: AlertOption) => void,
};

export const AlertContext: React.Context<Alert> = React.createContext({
    show: () => {},
} as Alert);

interface AlertProviderProps {
    children: React.ReactNode,
}

const useStyles = makeStyles((theme: Theme) => createStyles({
    root: {
        '& .MuiDialog-paperWidthSm': {
            minWidth: 320,
        },
    },
    title: {
        display: 'flex',
        alignItems: 'center',
        padding: theme.spacing(2),
        paddingBottom: 0,
        fontSize: theme.typography.h6.fontSize,
        fontWeight: theme.typography.fontWeightBold,
    },
    actions: {
        justifyContent: 'flex-end',
        padding: theme.spacing(2),
    },
    content: {
        width: '100%',
        maxHeight: 240,
        padding: theme.spacing(2),
        overflowY: 'auto',
    },
    icon: {
        color: theme.palette.common.white,
        padding: theme.spacing(1),
        borderRadius: theme.shape.borderRadius,
        marginRight: theme.spacing(2),
    },
}));

type AlertType = 'default' | 'success' | 'waring' | 'error';

type AlertOption = {
    title?: string,
    message?: string,
    type?: AlertType,
    onConfirm?: () => void,
    canceledOnTouchOutside?: boolean,
    cancelButton?: boolean,
};

export function AlertProvider(props: AlertProviderProps): React.ReactElement {
    const {
        children,
    } = props;
    const classes = useStyles();
    const theme = useTheme();

    const [open, setOpen] = useState<boolean>(false);
    const [type, setType] = useState<AlertType>('default');
    const [title, setTitle] = useState<string>('');
    const [canceledOnTouchOutside, setCanceledOnTouchOutside] = useState<boolean>(false);
    const [onConfirm, setOnConfirm] = useState<() => void>(() => () => {});
    const [message, setMessage] = useState<string>('');
    const [showCancelButton, setShowCancelButton] = useState(false);

    function show(option: AlertOption) {
        setType(option.type || 'default');
        setTitle(option.title || '');
        setMessage(option.message || '');
        setCanceledOnTouchOutside(option.canceledOnTouchOutside || false);
        if (option.onConfirm) {
            setOnConfirm(() => option.onConfirm);
        } else {
            setOnConfirm(() => () => {});
        }
        setShowCancelButton(option.cancelButton || false);
        setOpen(true);
    }

    function handleOnClose() {
        if (canceledOnTouchOutside) {
            setOpen(false);
        }
    }

    function handleConfirmClick() {
        setOpen(false);
        onConfirm();
    }

    function renderTitleIcon() {
        if (type === 'default') return '';

        let color = '';
        let icon: IconProp = faQuestion;
        switch (type) {
            case 'success':
                color = theme.palette.success.main;
                icon = faCheck;
                break;
            case 'waring':
                color = theme.palette.warning.main;
                icon = faExclamation;
                break;
            case 'error':
                color = theme.palette.error.main;
                icon = faTimes;
                break;
        }

        return (
            <div className={classes.icon} style={{ backgroundColor: color }}>
                <Icon fixedWidth icon={icon} />
            </div>
        );
    }

    return (
        <AlertContext.Provider value={{ show }}>
            <Dialog className={classes.root} open={open} onClose={handleOnClose}>
                {title ? (
                    <div className={classes.title}>
                        {renderTitleIcon()}
                        {title}
                    </div>
                ) : null}
                {message ? (
                    <div className={classes.content}>
                        <Typography color="textSecondary" style={{ whiteSpace: 'pre-wrap' }}>
                            {message}
                        </Typography>
                    </div>
                ) : null}
                <DialogActions className={classes.actions}>
                    {showCancelButton ? (
                        <Button
                            variant="outlined"
                            color="default"
                            disableElevation
                            onClick={() => setOpen(false)}
                        >
                            取消
                        </Button>
                    ) : null}
                    <Button
                        variant="contained"
                        color="secondary"
                        disableElevation
                        onClick={handleConfirmClick}
                    >
                        確定
                    </Button>
                </DialogActions>
            </Dialog>
            {children}
        </AlertContext.Provider>
    );
}
