import { useContext } from 'react';

import {
    AlertProvider,
    AlertContext,
    Alert,
} from './AlertProvider';

export const useAlert = (): Alert => useContext(AlertContext);
export { AlertProvider };
