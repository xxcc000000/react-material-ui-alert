import React, { useState, } from 'react';
import { Button, createStyles, Dialog, DialogActions, Typography, useTheme, } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { FontAwesomeIcon as Icon } from '@fortawesome/react-fontawesome';
import { faQuestion, faCheck, faExclamation, faTimes, } from '@fortawesome/pro-regular-svg-icons';
export var AlertContext = React.createContext({
    show: function () { },
});
var useStyles = makeStyles(function (theme) { return createStyles({
    root: {
        '& .MuiDialog-paperWidthSm': {
            minWidth: 320,
        },
    },
    title: {
        display: 'flex',
        alignItems: 'center',
        padding: theme.spacing(2),
        paddingBottom: 0,
        fontSize: theme.typography.h6.fontSize,
        fontWeight: theme.typography.fontWeightBold,
    },
    actions: {
        justifyContent: 'flex-end',
        padding: theme.spacing(2),
    },
    content: {
        width: '100%',
        maxHeight: 240,
        padding: theme.spacing(2),
        overflowY: 'auto',
    },
    icon: {
        color: theme.palette.common.white,
        padding: theme.spacing(1),
        borderRadius: theme.shape.borderRadius,
        marginRight: theme.spacing(2),
    },
}); });
export function AlertProvider(props) {
    var children = props.children;
    var classes = useStyles();
    var theme = useTheme();
    var _a = useState(false), open = _a[0], setOpen = _a[1];
    var _b = useState('default'), type = _b[0], setType = _b[1];
    var _c = useState(''), title = _c[0], setTitle = _c[1];
    var _d = useState(false), canceledOnTouchOutside = _d[0], setCanceledOnTouchOutside = _d[1];
    var _e = useState(function () { return function () { }; }), onConfirm = _e[0], setOnConfirm = _e[1];
    var _f = useState(''), message = _f[0], setMessage = _f[1];
    var _g = useState(false), showCancelButton = _g[0], setShowCancelButton = _g[1];
    function show(option) {
        setType(option.type || 'default');
        setTitle(option.title || '');
        setMessage(option.message || '');
        setCanceledOnTouchOutside(option.canceledOnTouchOutside || false);
        if (option.onConfirm) {
            setOnConfirm(function () { return option.onConfirm; });
        }
        else {
            setOnConfirm(function () { return function () { }; });
        }
        setShowCancelButton(option.cancelButton || false);
        setOpen(true);
    }
    function handleOnClose() {
        if (canceledOnTouchOutside) {
            setOpen(false);
        }
    }
    function handleConfirmClick() {
        setOpen(false);
        onConfirm();
    }
    function renderTitleIcon() {
        if (type === 'default')
            return '';
        var color = '';
        var icon = faQuestion;
        switch (type) {
            case 'success':
                color = theme.palette.success.main;
                icon = faCheck;
                break;
            case 'waring':
                color = theme.palette.warning.main;
                icon = faExclamation;
                break;
            case 'error':
                color = theme.palette.error.main;
                icon = faTimes;
                break;
        }
        return (<div className={classes.icon} style={{ backgroundColor: color }}>
                <Icon fixedWidth icon={icon}/>
            </div>);
    }
    return (<AlertContext.Provider value={{ show: show }}>
            <Dialog className={classes.root} open={open} onClose={handleOnClose}>
                {title ? (<div className={classes.title}>
                        {renderTitleIcon()}
                        {title}
                    </div>) : null}
                {message ? (<div className={classes.content}>
                        <Typography color="textSecondary" style={{ whiteSpace: 'pre-wrap' }}>
                            {message}
                        </Typography>
                    </div>) : null}
                <DialogActions className={classes.actions}>
                    {showCancelButton ? (<Button variant="outlined" color="default" disableElevation onClick={function () { return setOpen(false); }}>
                            取消
                        </Button>) : null}
                    <Button variant="contained" color="secondary" disableElevation onClick={handleConfirmClick}>
                        確定
                    </Button>
                </DialogActions>
            </Dialog>
            {children}
        </AlertContext.Provider>);
}
