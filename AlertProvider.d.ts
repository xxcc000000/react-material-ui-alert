import React from 'react';
export declare type Alert = {
    show: (option: AlertOption) => void;
};
export declare const AlertContext: React.Context<Alert>;
interface AlertProviderProps {
    children: React.ReactNode;
}
declare type AlertType = 'default' | 'success' | 'waring' | 'error';
declare type AlertOption = {
    title?: string;
    message?: string;
    type?: AlertType;
    onConfirm?: () => void;
    canceledOnTouchOutside?: boolean;
    cancelButton?: boolean;
};
export declare function AlertProvider(props: AlertProviderProps): React.ReactElement;
export {};
