import { AlertProvider, Alert } from './AlertProvider';
export declare const useAlert: () => Alert;
export { AlertProvider };
