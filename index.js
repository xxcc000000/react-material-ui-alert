import { useContext } from 'react';
import { AlertProvider, AlertContext, } from './AlertProvider';
export var useAlert = function () { return useContext(AlertContext); };
export { AlertProvider };
